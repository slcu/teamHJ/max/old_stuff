import morphsnakes
from tifffile import imsave # for saving the fit as .tiff
import ms_fast as ms
from tissueviewer.tvtiff import tiffread
import pickle
import numpy as np
import time
from handy_functions import *



###---------------------script------------------------------###
print 'lets go'

# image, tags = readImages('t1.tif')
# macwe = ms.MorphACWE(image, smoothing=3, lambda1=1, lambda2=3)
# macwe.levelset = circle_levelset(image.shape, (50, 100, 100), 100)
# macwe.step1(5)
# # t1 = macwe._u
# save_var(macwe._u,'t1.var')
# print 't1 done'
# 
# 
# image, tags = readImages('newtiff/t1.tif')
# macwe = ms.MorphACWE(image, smoothing=3, lambda1=1, lambda2=3)
# macwe.levelset = circle_levelset(image.shape, (100, 350, 350), 300)
# macwe.step1(5)
# # nt1 = macwe._u
# save_var(macwe._u,'nt1.var')
# print 'nt1 done'


image, tags = readImages('newtiff/t2.tif')
macwe = ms.MorphACWE(image, smoothing=3, lambda1=1, lambda2=3)
macwe.levelset = circle_levelset(image.shape, (100, 350, 350), 350)
macwe.run_fast(5, 5)
#macwe.step()
# nt2 = macwe._u
imsave('fits/nt2',np.array(macwe._u,'uint16'))
print 'nt2 done'

# 
# image, tags = readImages('newtiff/t3.tif')
# macwe = ms.MorphACWE(image, smoothing=3, lambda1=1, lambda2=3)
# macwe.levelset = circle_levelset(image.shape, (100, 350, 350), 350)
# macwe.step1(5)
# # nt3 = macwe._u
# save_var(macwe._u,'nt3.var')
# print 'nt3 done'


