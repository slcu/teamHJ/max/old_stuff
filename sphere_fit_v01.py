import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from tissueviewer.mesh import tvMeshImageVTK
import fast_fit as ff
from handy_functions import *
import vtk
from vtk.util.numpy_support import vtk_to_numpy, numpy_to_vtk
from openalea.container.topomesh_algo import border
from numpy import dtype
from scipy.ndimage import binary_dilation, binary_erosion, binary_closing
from scipy.ndimage.morphology import binary_opening
from reportlab.lib.randomtext import objects
import scipy.optimize as opt
from mayavi import mlab



def array_from_vtk_polydata(poly,size=[]):
    if np.shape(size) == np.shape([]):
        size = np.array(poly.GetPoints().GetBounds(),dtype='int')[1::2]
    indices = np.array(vtk_to_numpy(poly.GetPoints().GetData()),dtype='int')
    out = np.zeros(size)
    out[indices[:,0]-1,indices[:,1]-1,indices[:,2]-1] = 1
    return np.array(out)


"""
read polydata
"""
reader = vtk.vtkXMLPolyDataReader()
reader.SetFileName('reference_images/SAM500_cs.vtp')
reader.Update()
SAM = reader.GetOutput()


"""
color setup
"""
# Colour transfer function.
colorsteps = 10
ctf = vtk.vtkColorTransferFunction()
ctf.SetColorSpaceToDiverging()
ctf.AddRGBPoint(0.0, 0.230, 0.299, 0.754)
ctf.AddRGBPoint(1.0, 0.706, 0.016, 0.150)
cc = list()
for i in range(colorsteps):
    cc.append(ctf.GetColor(float(i) / float(colorsteps-1.))) 
    
# Lookup table.
lut = vtk.vtkLookupTable()
lut.SetNumberOfColors(colorsteps)
for i, item in enumerate(cc):
    lut.SetTableValue(i, item[0], item[1], item[2], 1.0)
    lut.SetRange(-.01, .01)
lut.Build()

"""
curvature
"""
curvature = vtk.vtkCurvatures()
curvature.SetCurvatureTypeToMean()
curvature.SetInput(SAM)
curvature.Update()



"""
Threshold Filter
"""
borders = vtk.vtkThreshold()
borders.ThresholdByLower(0.)
borders.SetInputConnection(curvature.GetOutputPort())


geofilter = vtk.vtkGeometryFilter()
geofilter.SetInputConnection(borders.GetOutputPort())
geofilter.Update()





"""
Connectivity Filter
"""
connect = vtk.vtkPolyDataConnectivityFilter()
connect.SetExtractionModeToPointSeededRegions()
connect.SetInputConnection(geofilter.GetOutputPort())
connect.Update()
points = connect.GetInput().GetNumberOfPoints()

res = 50
thresh = int(points/float(res))
objects_vtk = []
lastobj = []

for i in range(2,res):
    connect.InitializeSeedList()
    connect.AddSeed(int(points/res*float(i))-1)
    connect.Update()
    temp = []
    temp = connect.GetOutput()
    temp.Update()
    if temp.GetNumberOfPoints() > thresh:
        if not(temp.GetNumberOfPoints() in lastobj):
            objects_vtk.append(vtk.vtkPolyData())
            objects_vtk[-1].DeepCopy(temp)
            lastobj.append(temp.GetNumberOfPoints())


objects_vtk = np.flipud(sort_a_along_b(objects_vtk, lastobj))


"""
fit spheres
"""
colors = [(1.,1.,1.),(1.,0.,0.),(0.,1.,0.),(0.,0.,1.),(1.,1.,0.),(1.,0.,1.),(0.,1.,1.),(1.,0.5,0.5),(0.5,1.,0.5),(0.5,0.5,1.),(1.,1.,0.5),(1.,0.5,1.),(0.5,1.,1.)]


def list_sphere_fit(vtk_list):
    out = []
    for i in range(np.shape(vtk_list)[0]):
        out.append(fit_sphere(array_from_vtk_polydata(vtk_list[i])))
    return np.array(out)

spheres = list_sphere_fit(objects_vtk)

spheresSources = []
spheresObjects = []
sphereMappers = []
sphereActors = []

sphereResolution = 50

for i in range(np.shape(spheres)[0]):
    spherevtk = vtk.vtkSphereSource()
    spherevtk.SetCenter(spheres[i,0], spheres[i,1], spheres[i,2])
    spherevtk.SetRadius(spheres[i,3])
    spherevtk.SetThetaResolution(sphereResolution)
    spherevtk.SetPhiResolution(sphereResolution)
    spherevtk.Update()
    spheresSources.append(spherevtk.GetOutput())
#     spherePoly = vtk.vtkPolyData()
#     spherePoly.SetInput(spherevtk.GetOutput())
#     spherePoly.Update()
#     spheresObjects.append(vtk.vtkPolyData())
#     spheresObjects[-1].DeepCopy(spherevtk)
    
    mapper = vtk.vtkPolyDataMapper()
    mapper.SetInput(spheresSources[i])
    mapper.ScalarVisibilityOff()
    mapper.Update()
    sphereMappers.append(mapper)
    actor = vtk.vtkActor()
    actor.SetMapper(sphereMappers[i])
    actor.GetProperty().SetColor(colors[i])
    sphereActors.append(actor)

    





"""
Mapper,actor
"""
Mappers = []
Actors = []


colors = [(.7,.7,.7),(.7,0.,0.),(0.,.7,0.),(0.,0.,.7),(.7,.7,0.),(.7,0.,.7),(0.,.7,.7),(.7,0.4,0.4),(0.4,.7,0.4),(0.4,0.4,.7),(.7,.7,0.4),(.7,0.4,.7),(0.4,.7,.7)]




for i in range(np.shape(objects_vtk)[0]):
    mapper = vtk.vtkPolyDataMapper()
    mapper.SetInput(objects_vtk[i])
    mapper.ScalarVisibilityOff()
    mapper.Update()
    Mappers.append(mapper)
    actor = vtk.vtkActor()
    actor.SetMapper(Mappers[i])
    actor.GetProperty().SetColor(colors[i])
    Actors.append(actor)

"""
Renderer and Render Window
"""
 
render = vtk.vtkRenderer()
for i in range(np.shape(objects_vtk)[0]):
    render.AddActor(Actors[i])
    render.AddActor(sphereActors[i])

renderwindow = vtk.vtkRenderWindow()
renderwindow.AddRenderer(render)
renderwindow.SetSize(600,600)
interactrender = vtk.vtkRenderWindowInteractor()
interactrender.SetRenderWindow(renderwindow)
 
 
interactrender.Initialize()


    
axes = vtk.vtkAxesActor()
widget = vtk.vtkOrientationMarkerWidget()
widget.SetOutlineColor( 0.9300, 0.5700, 0.1300 )
widget.SetOrientationMarker( axes )
widget.SetInteractor( interactrender )
widget.SetViewport( 0.0, 0.0, 0.4, 0.4 )
widget.SetEnabled( 1 )
widget.InteractiveOn()

render.ResetCamera();
renderwindow.Render();
 
 


interactrender.Start()



exit()