# -*- coding: utf-8 -*-

"""
fast_fit
===========
"""
from scipy.ndimage.morphology import binary_opening, generate_binary_structure,\
    binary_fill_holes
__author__ = "P. Márquez Neila <p.mneila@upm.es>, Max Brambach <max.brambach.0065@student.lu.se>"


from itertools import cycle
import numpy as np
from scipy.ndimage import binary_dilation, binary_erosion, binary_closing
from scipy.ndimage.filters import gaussian_filter
from scipy.spatial import KDTree
from mayavi import mlab
from tissueviewer.tvtiff import tiffread

from handy_functions import *


'''
---------------------------- function cycle class --------------------------
'''
class fcycle(object):
    def __init__(self, iterable):
        """Call functions from the iterable each time it is called."""
        self.funcs = cycle(iterable)
    def __call__(self, *args, **kwargs):
        f = next(self.funcs)
        return f(*args, **kwargs)   
'''
---------------------------- SI and IS operators --------------------------
'''
"""
P is used in the smoothing algorithm (IS and SI operators). See fig 3 in Marquez-Neila et al. 2014. (DOI: 10.1109/TPAMI.2013.106)
"""
P = [np.zeros((3,3,3)) for i in range(9)]
P[0][:,:,1] = 1
P[1][:,1,:] = 1
P[2][1,:,:] = 1
P[3][:,[0,1,2],[0,1,2]] = 1
P[4][:,[0,1,2],[2,1,0]] = 1
P[5][[0,1,2],:,[0,1,2]] = 1
P[6][[0,1,2],:,[2,1,0]] = 1
P[7][[0,1,2],[0,1,2],:] = 1
P[8][[0,1,2],[2,1,0],:] = 1
_aux = np.zeros((0))

def SI(u,iterate=1):
    """SI operator."""
    global _aux
    global P
    if u.shape != _aux.shape[1:]:
        _aux = np.zeros((len(P),) + u.shape)    
    for i in range(len(P)):
        _aux[i] = binary_erosion(u, P[i],iterations=iterate,mask=getedges(np.shape(u)))    
    return _aux.max(0)

def IS(u,iterate=1):
    """IS operator."""
    global _aux
    global P
    if u.shape != _aux.shape[1:]:
        _aux = np.zeros((len(P),) + u.shape)
    
    for i in range(len(P)):
        _aux[i] = binary_dilation(u, P[i],iterations=iterate,mask=getedges(np.shape(u)))
    return _aux.min(0)
# SIoIS operator.
SIoIS = lambda u: SI(IS(u))
ISoSI = lambda u: IS(SI(u))
curvop = fcycle([SIoIS, ISoSI])

'''
-------------------poincloud class------------------------------
'''
class pointcloud(object):
    """
    Class used for fitting the surface of a confocal stack of images. Uses a threshold based approach and the smoothing algorithm of Marquez-Neila et al. 2014.
    The class has a kit structure. 
    Objects of this class have two properties:
        - data: original data (e.g. confocal images as np.array)    
        - model: model of the surface; initially None, is created by the .fit() method    
    Use like this:
        - Create object: obj = pointcloud(data)
        - Create model from data: obj.fit()
        - Smooth model: obj.smooth()
        - Visualise model: obj.show3d()
    """
    
    def __init__(self, data=None,model=None):
        self.model = model
        self.data = data
        
    def shape(self):
        return np.array(np.shape(self.data))
    
    def fit(self,threshold=1):
        """
        Uses a threshold based approach to generate the initial model of obj.data. Threshold default is half of the images mean intensity; can be adjusted.
        """
        self.model = self.data > threshold*0.5*np.mean(self.data.flatten())
                    
    def smooth(self,iterate=1):
        """
        Uses the ISoSI operator of Marquez-Neila et al. 2014 to smooth the object and to form the surface. At least one iteration is needed to get a good fit.
        """
        res = self.model
        for i in range(iterate):
            res = curvop(res)
        self.model = res
        
    def smooth_gauss(self,sigma=2):
        self.model = gaussian_filter(self.model,sigma)
        
    def dilation(self,iterate=1,pattern=[]):
        """
        Dilation method to close holes on the surface. A structuring element can be specified as pattern.
        """
        if np.shape(pattern) == (0,):
            pattern = generate_binary_structure(3,1)
        self.model = binary_dilation(self.model,pattern,iterations=iterate)
    
    def erosion(self,iterate=1,pattern=[]):
        """
        Erosion method to remove dots. A structuring element can be specified as pattern.
        """
        if np.shape(pattern) == (0,):
            pattern = generate_binary_structure(3,1)
        self.model = binary_erosion(self.model,pattern,iterations=iterate)
        
    def closing(self,iterate=1,pattern=[]):
        """
        Closing method. A structuring element can be specified as pattern.
        """
        if np.shape(pattern) == (0,):
            pattern = generate_binary_structure(3,1)
        self.model = binary_closing(self.model,pattern,iterations=iterate)
        
    def opening(self,iterate=1,pattern=[]):
        """
        Opening method. A structuring element can be specified as pattern.
        """
        if np.shape(pattern) == (0,):
            pattern = generate_binary_structure(3,1)
        self.model = binary_opening(self.model,pattern,iterations=iterate)
        
    def fill_holes(self,pattern=[]):
        """
        Closing method. A structuring element can be specified as pattern.
        """
        if np.shape(pattern) == (0,):
            pattern = generate_binary_structure(3,1)
        self.model = binary_fill_holes(self.model,pattern)
                
    def reduce(self,factor=2):
        """
        Reduces the model and data size by the specified factor. The method deletes the unused planes (keeps only every nth - n=factor).
        """
        if type(self.data) != type(None):
            self.data = self.data[::factor,::factor,::factor]
        if type(self.model) != type(None):
            self.model = self.model[::factor,::factor,::factor]
        
    def step(self,lambda1,lambda2):
        """Perform a single step of the morphological Chan-Vese evolution."""
        # Assign attributes to local variables for convenience.
        if type(self.model) == type(None):
            raise ValueError("the levelset function is not set (use .model = )")
                
        # Determine c0 and c1.
        inside = self.model>0
        outside = self.model<=0
        c0 = self.data[outside].sum() / float(outside.sum())
        c1 = self.data[inside].sum() / float(inside.sum())
        
        # Image attachment.
        dres = np.array(np.gradient(self.model))
        abs_dres = np.abs(dres).sum(0)
#         print 'abs_dres',abs_dres
        #aux = abs_dres * (c0 - c1) * (c0 + c1 - 2*data)
        aux = abs_dres * (lambda1*(self.data - c1)**2 - lambda2*(self.data - c0)**2)
#         print 'aux',aux
        self.model[aux < 0] = 1
        self.model[aux > 0] = 0
#       
    def surf_grad(self,sign=True):
        if sign == True:
            self.model = np.sum(np.gradient(self.model),axis=0) < 0
        elif sign == False:
            self.model = np.sum(np.gradient(self.model),axis=0) > 0
        elif sign == None:
            self.model = np.sum(np.gradient(self.model),axis=0) != 0
            
    def surf(self):
        kernel = np.zeros((2,2,2))
        kernel[:,:,1] = 1
        kernel[:,1,:] = 1
        kernel[1,:,:] = 1
        self.model = np.logical_and(self.model, np.logical_not(binary_erosion(self.model, kernel,mask = getplanes(np.shape(self.model),onezero=False))))
        
    def run(self, iterations,lambda1,lambda2,iterate_smooth):
        """Run several iterations of the morphological Chan-Vese method."""
        for i in range(iterations):
            self.step(lambda1,lambda2)
            self.smooth(iterate_smooth)
            print("Iteration %s/%s..." % (i + 1, iterations))
            
    def invert(self):
        self.model = np.logical_not(self.model)
        
    def show3d(self,invert=False):
        """
        Visualises the model.
        """
        if self.model.any != None:
            if invert == False:
                mlab.gcf()
                mlab.clf()
                src = mlab.pipeline.scalar_field(self.data)
                mlab.pipeline.image_plane_widget(src, plane_orientation='x_axes', colormap='gray')
                #mlab.contour3d(np.array(self.model,dtype='int'), contours=[0.5])
                mlab.points3d(np.nonzero(self.model)[0],np.nonzero(self.model)[1],np.nonzero(self.model)[2],scale_factor=.5)
            if invert == True:
                mlab.gcf()
                mlab.clf()
                src = mlab.pipeline.scalar_field(self.data)
                mlab.pipeline.image_plane_widget(src, plane_orientation='x_axes', colormap='gray')
                #mlab.contour3d(np.array(self.model,dtype='int'), contours=[0.5])
                mlab.points3d(np.nonzero(np.logical_not(self.model))[0],np.nonzero(np.logical_not(self.model))[1],np.nonzero(np.logical_not(self.model))[2],scale_factor=.5)

            mlab.show()
        else:
            print 'No model data to show!'
    
    def surface_scan(self,directions=[1,0,0,0,0,0]):
        u = self.model
        u = np.array(u,dtype='bool')
        [z,y,x] = np.shape(u)
        unew = np.zeros([z,y,x,6],dtype='bool')
        maskplanez = np.ones((y,x),dtype='bool')
        maskplaney = np.ones((z,x),dtype='bool')
        maskplanex = np.ones((z,y),dtype='bool')
        if directions[0] == 1:
            for i in range(z):
                i=-i-1
                unew[i,:,:,0] = np.logical_and(u[i,:,:],maskplanez)
                maskplanez = np.logical_and((maskplanez), np.logical_not(unew[i,:,:,0]))
        if directions[1] == 1:    
            for i in range(z):
                i=i
                unew[i,:,:,1] = np.logical_and(u[i,:,:],maskplanez)
                maskplanez = np.logical_and(maskplanez, np.logical_not(unew[i,:,:,1]))
        if directions[2] == 1:
            for i in range(y):
                i=-i-1
                unew[:,i,:,2] = np.logical_and(u[:,i,:],maskplaney)
                maskplaney = np.logical_and(maskplaney, np.logical_not(unew[:,i,:,2]))
        if directions[3] == 1:
            for i in range(y):
                unew[:,i,:,3] = np.logical_and(u[:,i,:],maskplaney)
                maskplaney = np.logical_and(maskplaney, np.logical_not(unew[:,i,:,3]))
        if directions[4] == 1:    
            for i in range(x):
                i=-i-1
                unew[:,:,i,4] = np.logical_and(u[:,:,i],maskplanex)
                maskplanex = np.logical_and(maskplanex, np.logical_not(unew[:,:,i,4]))
        if directions[5] == 1:
            for i in range(x):
                unew[:,:,i,5] = np.logical_and(u[:,:,i],maskplanex)
                maskplanex = np.logical_and(maskplanex, np.logical_not(unew[:,:,i,5]))
        self.model = np.array(np.sum(unew,axis=3) != 0,dtype='bool')

    def run_visual(self,num_iters=20,lambda1=1,lambda2=1,smooth=3,invert=True):
        fig = mlab.gcf()
        mlab.clf()
        src = mlab.pipeline.scalar_field(self.data)
        mlab.pipeline.image_plane_widget(src, plane_orientation='x_axes', colormap='gray')
        if invert == False:
            cnt = mlab.points3d(np.nonzero(self.model)[0],np.nonzero(self.model)[1],np.nonzero(self.model)[2])
            @mlab.animate(ui=True)
            def anim():
                for i in range(num_iters):
                    self.step(lambda1,lambda2)
                    self.smooth(smooth)
                    cnt.mlab_source.reset(x=np.nonzero(self.model)[0],y=np.nonzero(self.model)[1],z=np.nonzero(self.model)[2])
                    print("Iteration %s/%s..." % (i + 1, num_iters))
                    yield
            anim()
            mlab.show()
        if invert == True:
            cnt = mlab.points3d(np.nonzero(np.logical_not(self.model))[0],np.nonzero(np.logical_not(self.model))[1],np.nonzero(np.logical_not(self.model))[2])
            @mlab.animate(ui=True)
            def anim():
                for i in range(num_iters):
                    self.step(lambda1,lambda2)
                    self.smooth(smooth)
                    cnt.mlab_source.resetmlab.points3d(x=np.nonzero(np.logical_not(self.model))[0],y=np.nonzero(np.logical_not(self.model))[1],z=np.nonzero(np.logical_not(self.model))[2])
                    print("Iteration %s/%s..." % (i + 1, num_iters))
                    yield
            anim()
            mlab.show()
    
"""
---------------------------------------------functions for poincloud class -------------------------------------------------
"""

def kdt_compare(pointcloudA,pointcloudB,leafsize=100):
    """
    Uses the k-d tree method to compare two pointclouds. Also fast_fit.poincloud objects can be processed.
    A is compared to B.
    Returns the average distance of a point in A to B (the sum of distances devided by the number of points).
    """
    pointcloudA = check_model_and_return(pointcloudA)
    pointcloudB = check_model_and_return(pointcloudB)
    A = np.array(np.nonzero(pointcloudA)).T
    B = np.array(np.nonzero(pointcloudB)).T
    kdt = KDTree(B,leafsize)
    distances,_ = kdt.query(A)
    return np.sum(distances)/np.array(np.shape(A),dtype='float')[0]

def bf_compare(pointcloudA,pointcloudB):
    """
    Uses a brute force method to compare two pointclouds. Also fast_fit.poincloud objects can be processed.
    A is compared to B.
    Returns the average distance of a point in A to B (For each point in A, the distances to all points in B are computed and the minimal distances are summed and then devided by the number of points in A).
    """
    pointcloudA = check_model_and_return(pointcloudA)
    pointcloudB = check_model_and_return(pointcloudB)
    indices1 = np.array(np.nonzero(pointcloudA))
    indices2 = np.array(np.nonzero(pointcloudB))
    _,points = np.shape(indices1)
    distance = 0
    for i in range(points):
        distance += np.sqrt(np.min(np.sum(np.square(indices2.T - indices1[:,i]),axis=1)))  
    return distance/points
        
def samepoints(pointcloudA,pointcloudB):
    """
    Returns the number of points that are at the same position in A and B normalised to the number of points in A.
    """
    pointcloudA = check_model_and_return(pointcloudA)
    pointcloudB = check_model_and_return(pointcloudB)
    return np.sum(np.logical_and(pointcloudA,pointcloudB).flatten())/float(np.shape(np.nonzero(pointcloudA))[1])

def setedges(array,value=0):
    """
    Sets the edges of a 3d array to a specified value.
    """
    array[[0,0,-1,-1],[0,-1,0,-1],:] = value
    array[:,[0,0,-1,-1],[0,-1,0,-1]] = value
    array[[0,-1,0,-1],:,[0,0,-1,-1]] = value
    return array

def getedges(shape):
    """
    Creates an 3d array with zeros on the edges and ones else. Used to suppress the fitting of edges by the smooth() function (ISoSI operator mask).
    """
    array = np.ones(shape)
    setedges(array)
    return array

def setplanes(array,value=0):
    array[[0,-1],:,:] = value
    array[:,[0,-1],:] = value
    array[:,:,[0,-1]] = value
    
def getplanes(shape,onezero=True):
    if onezero == True:
        array = np.zeros(shape)
        setplanes(array,value=1)
    if onezero == False:
        array = np.ones(shape)
        setplanes(array,value=0)
    return array
    
def check_model_and_return(PC):
    """
    Checks if input is poincloud object and returns its model data, if available. Else returns input.
    """
    if type(PC) == type(pointcloud(())):
        if PC.model != None:
            return PC.model
        else:
            print 'No model data to process!'
    else:
        return PC 
def readImages(imageFileName):
    """
    Reads data of a .tif file from specified location and returns the image data and the tags.
    """
    image, tags = tiffread(imageFileName)
    return image, tags

    